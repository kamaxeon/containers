#!/bin/bash
#
# Generate minimal container image for Fedora ELN
#

set -euo pipefail

function echo_green { echo -e "\e[1;32m${1}\e[0m"; }

if [[ -v DEBUG ]]; then
    set -x
fi

base_url="https://odcs.fedoraproject.org/composes/production/latest-Fedora-ELN/compose"

# Write all image metadata in the docker format, not the standard OCI format.
# Newer versions of docker can handle the OCI format, but older versions, like
# the one shipped with Fedora 30, cannot handle the format.
export BUILDAH_FORMAT=docker

# The name for the system's architecture (uname -m) isn't always the same as
# the repos/packages, so we need to define two variables and change the
# exceptions: x86_64/amd64 and arm64/aarch64
if ! [[ -v IMAGE_ARCH ]]; then
    ARCH="x86_64"
    IMAGE_ARCH="amd64"
elif [[ "${IMAGE_ARCH}" == "amd64" ]]; then
    # For amd64, the packages uses x86_64 as arch
    ARCH="x86_64"
elif [[ "${IMAGE_ARCH}" == "arm64" ]]; then
    # For arm64, the packages uses aarch64 as arch
    ARCH="aarch64"
else
    ARCH="${IMAGE_ARCH}"
fi

# Start new container from scratch
echo_green "Create a new container from the scratch"
newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount "${newcontainer}")

# Install the packages
# Also check out the ELN compose and package info at
#       https://tiny.distro.builders/view--view-eln.html
# Proves pretty useful to know if some packages are broken or removed on purpose
echo_green "Install the Fedora ELN base packages"
dnf install --installroot="${scratchmnt}" \
    --releasever=eln \
    --forcearch="$ARCH" \
    --assumeyes \
    --disablerepo=* \
    --repofrompath=eln-baseos,"${base_url}/BaseOS/${ARCH}/os/" \
    --repofrompath=eln-appstream,"${base_url}/AppStream/${ARCH}/os/" \
    --repofrompath=eln-everything,"${base_url}/Everything/${ARCH}/os/" \
    --enablerepo=eln-baseos \
    --enablerepo=eln-appstream \
    --enablerepo=eln-everything \
    --nogpgcheck \
    --setopt=tsflags=nodocs \
    --setopt=install_weak_deps=False \
    --nodocs \
    --disableplugin=versionlock \
    bash \
    coreutils \
    dnf \
    dnf-plugins-core \
    fedora-release-eln \
    fedora-repos-eln \
    findutils \
    glibc-minimal-langpack \
    rootfiles \
    rpm \
    shadow-utils \
    sssd-client \
    sudo \
    tar \
    util-linux \
    vim-minimal \
    yum

# Set install langs macro so that new rpms that get installed will
# only install langs that we limit it to.
echo_green "Configure the language"
LANG="en_US"
echo "%_install_langs $LANG" > "${scratchmnt}/etc/rpm/macros.image-language-conf"

echo 'LANG="C.UTF-8"' > "${scratchmnt}/etc/locale.conf"

# https://bugzilla.redhat.com/show_bug.cgi?id=1400682
echo_green "Import RPM GPG key"
chroot "${scratchmnt}" rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-33-primary \
                                  /etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-34-primary

# Disable conflicting repositories.
echo_green "Disable the conflicting repositories"
chroot "${scratchmnt}" dnf config-manager --disable "*rawhide*" "*cisco*"

# Re-Enable ELN - Everything repository
# The package fedora-repos-eln doesn't include this repo config
echo_green "Re-enable the ELN Everything repository"
cat <<EOF >> "${scratchmnt}/etc/yum.repos.d/fedora-eln.repo"
[eln-everything]
name=Fedora - ELN Everything - Developmental packages for the next Enterprise Linux release
baseurl=https://odcs.fedoraproject.org/composes/production/latest-Fedora-ELN/compose/Everything/\$basearch/os/
#metalink=https://mirrors.fedoraproject.org/metalink?repo=eln&arch=\$basearch
enabled=1
countme=1
metadata_expire=6h
repo_gpgcheck=0
type=rpm
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-\$releasever-\$basearch
skip_if_unavailable=False

EOF

echo "# fstab intentionally empty for containers" > "${scratchmnt}/etc/fstab"

# Remove machine-id on pre generated images
rm -f "${scratchmnt}/etc/machine-id"
touch "${scratchmnt}/etc/machine-id"

# https://pagure.io/atomic-wg/issue/308
printf "tsflags=nodocs\n" >> "${scratchmnt}/etc/dnf/dnf.conf"

# Final pruning
echo_green "Cleaning and pruning the system"
if [ -d "${scratchmnt}" ]; then
    rm -rfv "${scratchmnt}/var/cache/"* \
         "${scratchmnt}/var/log/"* \
         "${scratchmnt}/tmp/"*
fi

# Removing all locales but en_US
KEEPLANG=en_US
for dir in locale i18n; do
    chroot "${scratchmnt}" \
        find "/usr/share/${dir}" -mindepth  1 -maxdepth 1 -type d \
        -not \( -name "${KEEPLANG}" -o -name POSIX \) -exec rm -rfv {} +
done

# Configure container command, env, labels and author
echo_green "Configure the container image metadata"
buildah config \
    --cmd /bin/bash \
    --env DISTTAG=elncontainer \
    --env FGC=eln \
    --env container=docker \
    --label name="${IMAGE}" \
    --label vendor="Fedora project" \
    --label version=eln \
    --label license=MIT \
    --arch "$IMAGE_ARCH" \
    --created-by="Buildah from scratch" \
    --author="CKI Project Team <cki-project@redhat.com>" \
    "${newcontainer}"

# Unmount, commit to the final image and remove the container
echo_green "Closing the image"
buildah unmount "${newcontainer}"
buildah commit "${newcontainer}" "${IMAGE}"
buildah rm "${newcontainer}"

echo_green "Finish"
