# CKI Project Container Images

This repo contains the build scripts for the container images used by the CKI
Project. Each container image is built and hosted in GitLab CI using
[buildah](https://github.com/containers/buildah).

A thorough description on how the CKI project uses container images can be
found in the [public documentation].

Please only add new documentation here that is specific to the CKI container
image repository!

## Project Details

### Maintainers

[Iñaki Malerba](https://gitlab.com/inakimalerba)
[Michael Hofmann](https://gitlab.com/mh21)

### Design principles

- Only container image build files belong in the `builds` directory. Any
  additional content, such as certificates, repositories, or configuration
  files must be in the `files` directory.
- Keep container images small by cleaning up after yourself within the
  container. Watch out for applications that leave behind lots of cached data.

## Container image include files

The container image build files are split into multiple include files to create
some orthogonality and reduce duplication. This is a work in progress.

In each container image build file, exactly one of the setup include files
should be used via the first `#include` directive. This is mainly concerned
with setting up common dnf/yum configuration, certificates and environment
variables and installing a core Python 3 stack:

- `setup-base-public`: Common setup steps that should be included in all
  Fedora-based CKI container images.
  This includes the `envvars` and `certs` include files.
- `setup-base-rhel`: Common setup steps that should be included in all
  RHEL-based builder images.
  This includes the `envvars` and `certs` include files.
- `setup-base-stream`: Common setup steps that should be included in all
  CentOS Stream-based builder images.
  This includes the `envvars` and `certs` include files.

> **NOTE:** It's important not to use any Dockerfile/Containerfile command
> (`RUN`, `COPY`, `ADD`, etc) before this first `#include` or it will break
> the build.

All setup include files include the following files:

- `envvars`: configure common environment variables
- `certs`: add Red Hat and CKI SSL certificates to the CA trust store

Configuration of pipeline container images is split across the following files:

- `builder-public`: Common steps for the Fedora-based pipeline builder images.
  This includes the `builder-all` include file.
- `builder-7`: Common steps for all builder images based on RHEL 7.
  This includes the `builder-all` include file.
- `builder-8`: Common steps for all builder images based on RHEL/CentOS Stream 8.
  This includes the `builder-all` include file.
- `builder-9`: Common steps for all builder images based on RHEL/CentOS Stream 9.
  This includes the `builder-all` include file.
- `builder-all`: Common steps for all pipeline builder images.
  This includes the `pipeline` include file.
- `pipeline`: Common steps for all pipeline images.
- `setup-pipeline`: Extra common setup steps that should be included in all
  **Fedora-based** images used in the pipeline.

Each container image build file should end with an `#include` directive for the
`cleanup` include file which takes care of common cleanup tasks such as
removing caches from dnf and pip.

The `setup-base` and `python-requirements` include files should only be used
for building application container images, i.e. not in one of the container
image repositories.

### Container image for ELN

The images are created from an official base image, usually that image is
`registry.fedoraproject.org/fedora`, but for [ELN] there is no official image
yet, so TEMPORARILY another helper `build_eln_from_scratch.sh` is used, to
create that base image.

The helper is called by `cki_build_image.sh` when the `IMAGE_NAME` is
`builder-eln`, so the builder can be generated in the same manner as
the rest.

For creating the `builder-eln` locally, the following can be used:

```shell
podman run \
    --rm \
    -e IMAGE_NAME=builder-eln \
    --privileged \
    -w /code \
    -v .:/code \
    -v ~/.local/share/containers:/var/lib/containers \
    registry.gitlab.com/cki-project/containers/buildah:latest \
    cki_build_image.sh
```

For creating just the base image (for developing of debugging purposes),
something similar can be used:

```shell
podman run \
    --rm \
    --privileged \
    -w /code \
    -v .:/code \
    -v ~/.local/share/containers:/var/lib/containers \
    registry.gitlab.com/cki-project/containers/buildah:latest \
    build_eln_from_scratch.sh
```

That base image can be generated for different architectures (`amd64`,
`arm64`, `ppc64le` and `s390x`). By default, the script generates the
image for `amd64`, but it can be changed by passing the enviroment variable
`IMAGE_ARCH` with a different architecture name. For example, the following
would generate a `builder-eln` for `ppc64le`:

```shell
podman run \
    --rm \
    -e IMAGE_ARCH=ppc64le \
    --privileged \
    -w /code \
    -v .:/code \
    -v ~/.local/share/containers:/var/lib/containers \
    registry.gitlab.com/cki-project/containers/buildah:latest \
    build_eln_from_scratch.sh
```

**NOTE:** This is a temporary solution and it can be removed once the upstream
images are available. This work is tracked in <https://github.com/fedora-eln/eln/issues/16>.

More details about how the helper `build_eln_from_scratch.sh` works can be
found at [ELN_from_scratch.md](ELN_from_scratch.md).

**NOTE:** Check out the [ELN compose and package] info when running into
problems with packages.

## Native builders

The repository has native builders attached with tags following the pattern
`container-build-runner-{kernel-arch}`. These builders also have access to
RHEL7 and RHEL8 RPM repositories.

## Current container images

This repository currently provides the following container images:

### base

This container image is the preferred base image of derived container images
and only includes a minimal Python 3 and pip environment.

### buildah

This multi-arch container image can run buildah to build container images.

### builder-fedora

This container image can compile upstream Linux kernels and it also includes
extra packages for Python.

### builder-rawhide

Similar to `builder-fedora`, but based on a Rawhide environment.

### builder-eln

Similar to `builder-fedora`, but based on a [ELN] environment.

### builder-rhel[6789]

These container images can compile RHEL 6/7/8/9 Linux kernels.

#### zstream images

Some RHEL images have also zstream versions:

- RHEL 7:
  - 7.2
  - 7.3
  - 7.4
  - 7.6
  - 7.7
- RHEL 8:
  - 8.1
  - 8.2
  - 8.3
  - 8.4

### python

This multi-arch container image (only amd64/arm64) is used for all the
non-build stages of the CKI pipeline.

## Mirrored images

The container image registry connected to this repository also hosts a mirror of
various container images from Docker Hub. For details, consult the [public
documentation].

## Debugging

The container image definitions are made by `cpp preprocessor` style templates.
It allows the composition of different templates and reduce the duplications.
But, sometimes is hard to see the implications of changes to the templates, until
the changes are pushed to Gitlab and the CI pipeline tries to build the image.

To see how the final Dockerfile/Containerfile for one specific image looks like,
try the following commands:

<!-- markdownlint-disable MD014 -->
```shell
$ IMAGE="builder-fedora"
$ export CPATH=includes
$ cpp -E -traditional -undef builds/${IMAGE}.in | sed '/^$/d' > ${IMAGE}.Dockerfile
```
<!-- markdownlint-enable MD014 -->

For this case, `builder-fedora.Dockerfile` will be the resulting Dockerfile that
would be used to build the image. It can be used with `docker`, `podman` or
`buildah` to build the image locally and test it.

This is also useful for detecting errors with the `cpp preprocessor` templates
format. It sometimes has errors and we don't see it until the image is trying
to build at Gitlab.

Another useful technique for checking and improving the templates locally is linting
the generated Dockerfile. A tool like [Hadolint] can be used for this:

<!-- markdownlint-disable MD014 -->
```shell
$ hadolint ${IMAGE}.Dockerfile
```
<!-- markdownlint-enable MD014 -->

[public documentation]: https://cki-project.org/docs/hacking/contributing/container-images
[ELN]: https://docs.fedoraproject.org/en-US/eln/
[Hadolint]: https://hadolint.github.io/hadolint/
[ELN compose and package]: https://tiny.distro.builders/view--view-eln.html
